<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/all.css">
</head>
<body>
 <div id="carousel-example-generic" class="carousel slide carousel-fade carousel-sync" data-ride="carousel" data-pause="false" data-interval="5000">
    <div class="container-fluid">
        <div class="col-sm-10 col-sm-offset-1">
        	<div class="carousel-inner" role="listbox">
            	<div class="item active" style="height: 287px;">
                	<div class="col-sm-4 text-center">
                    	<img src="img/testimonials/digitales.png" width="200px" height="200px" class="img-responsive" title="digitales" alt="digitales">
                	</div>
                
                	<div class="col-sm-8 testimonial-description">
                    	<p class="text-justify">"New Indictrans delivers the business requirements we need in a competitive market. 

                        We at Digitales value"</p>
                        <p class="text-right"><i>- Digitales, Australia (Online Music Library)</i></p>
                        <span class="fa fa-quote-left"></span>
                    </div>
                </div>
                <div class="item" style="height: 287px;">
                 	<div class="col-sm-4 text-center ">
                    	<img src="img/testimonials/esds.png" width="200px" height="200px" class="img-responsive" title="esds" alt="esds">
                	</div>
                	<div class="col-sm-8 testimonial-description">
                    	<p class="text-justify">"We would like to express our satisfaction with the GIS software services provided by 

                        Indictrans. Indictrans Team is extremely talented and supportive."</p>
                        <p class="text-right"><i>- ESDS Software Solution Pvt Ltd, Nashik, India (Data Center Management)</i></p>
                        <span class="fa fa-quote-left"></span>
                    </div>
                </div>
                <div class="item" style="height: 287px;">
                 	<div class="col-sm-4 text-center ">
                    	<img src="img/testimonials/choice.png" width="200px" height="200px" class="img-responsive" title="choice" alt="choice">
                	</div>
                	<div class="col-sm-8 testimonial-description">
                    	<p class="text-justify">"Indictrans has been fantastic – specialized inventory control built directly into our 

                        ERP is just one of the ways they’ve helped to streamline our business operations."</p>
                        <p class="text-right"><i>- Energy Choice Inc., USA (Power Solutions)</i></p>
                        <span class="fa fa-quote-left"></span>
                    </div>
                </div>
                <div class="item" style="height: 287px;">
                 	<div class="col-sm-4  text-center">
                    	<img src="img/testimonials/ptp.png" width="200px" height="200px" class="img-responsive" title="pune traffic police" alt="pune traffic police">
                	</div>
                	<div class="col-sm-8 testimonial-description">
                    	<p class="text-justify">"We are glad to issue this letter of appreciation for the pioneering initiative taken by 

                        Indictrans in building GIS solution for traffic police department and public access. 

                        Apart from the technical competence and leadership skill, Indictrans has shown the 

                        commitment to the standards and Free / Open source with a desire to spread 

                        benefits of technology to the masses. Indictrans delivered the software in stipulated 

                        time and cost, something which is rare for

                        software project of this complexity."</p>
                        <p class="text-right"><i>- Dy. Commissioner of Police, Traffic, Pune</i></p>
                        <span class="fa fa-quote-left"></span>
                    </div>
                </div>
                <div class="item" style="height: 287px;">
	                <div class="col-sm-4 text-center ">
	                   <img src="img/testimonials/wr.png" width="200px" height="200px" class="img-responsive" title="western railway" alt="western railway">
	                </div>
	                <div class="col-sm-8 testimonial-description">
	                    <p class="text-justify">"Indictrans has developed SMS Integrated solution for Financial Management. This

	                        solution has strengthened the e-governance issues and has provided valuable linkage 

	                        to the issues of transparency in administration. I wish them all the luck with their 

	                        endeavors."</p>
	                        <p class="text-right"><i>- Senior EDP Manager, Western Railway, Churchgate, Mumbai</i></p>
	                        <span class="fa fa-quote-left"></span>
	                </div>
                </div>
                <div class="item" style="height: 287px;">
                 	<div class="col-sm-4 text-center ">
                    	<img src="img/testimonials/wr.png" width="200px" height="200px" class="img-responsive" title="western railway" alt="western railway">
                	</div>
                	<div class="col-sm-8 testimonial-description">
                    
                    <p class="text-justify">"I thanks Indictrans for their role in implementing Business Solution. Today our 

                        department remains glad in Indian Railway, to have such software system running 

                        for such long period."</p>
                        <p class="text-right"><i>- Sr. DPO, Personnel Department, Western Railway, Mumbai</i></p>
                        <span class="fa fa-quote-left"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
 </div> 					
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>