<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ERPNext</title>
	<!-- Bootstrap css -->
	<link href="css/bootstrap.min.css" rel="stylesheet">	
	<!-- Font awesome -->
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<!-- Custom Css -->
	<link type="text/css" rel="stylesheet" href="css/all.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#"><img src="img/logo/logo.png" alt="Logo" class="img-responsive logo" /></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right tab-links">
		        <li class="active"><a href="#" class="links">Home</a></li>
		        <li><a href="#" class="links">About Us</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Solutions <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">ERP Next</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="#">GIS</a></li>
		          </ul>
		        </li>
		        <li><a href="#" class="links">Career</a></li>
		        <li><a href="#" class="links">Clientele</a></li>
		        <li><a href="#" class="links">Contact</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
	<section class="erpnext-landing">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">
					<h1 class="section-heading1 text-center">
						<span>ERP Next</span>
					</h1>
				</div>
				<div class="col-sm-8 col-sm-offset-2">
					<p class="text-center">
					Business Solutions have become indispensable to functioning of an organization. Business Intelligence data and analysis hold the key to organizational future. NITPL offers cloud-based, customized, user-friendly and intelligence-intensive business solutions for small, medium and large organizations to empower them with total business control and 100% operational transparency.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="erpnext-description">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<h1 class="section-heading1 text-center">
						<span>
							NITPL SOLUTIONS ACROSS SECTORS
						</span>
					</h1>
				</div>
				<div class="col-sm-8">
					<div class="col-sm-12">
						<div class="solution-content">
							<div class="container-fluid">
								<div class="row content">
									<div class="col-sm-3">
										<div class="solution-number">
											1
										</div>
									</div>
									<div class="col-sm-9">
										<h3 class="text-uppercase"> 
											<strong>ERP for Manufacturing, Retail and Services Industry</strong>
										</h3>
										<ul class="list-unstyled">
											<li>
												<p class="grey">
												Sophisticated ERP Platform designed for all functions of small and medium enterprises.
												</p>
											</li>
											<li>
												<p class="grey">Optimized for Desktop as well as Mobile.
												</p>
											</li>
										</ul>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="solution-content">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-3">
										<div class="solution-number">
											2
										</div>
									</div>
									<div class="col-sm-9">
										<h3 class="text-uppercase"> 
											<strong>Loyalty platform engine</strong>
										</h3>
										<ul class="list-unstyled">
											<li>
												<p class="text-justify grey">
												An ERP solution that helps you manage your brand’s loyalty through reward loyalty program. It handles all activities from data collection to customer checkout related to Loyalty Points. This entire platform can be customized to optimize it for your brand.
												</p>
											</li>
										</ul>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="solution-content">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-3">
										<div class="solution-number">
											3
										</div>
									</div>
									<div class="col-sm-9">
										<h3 class="text-uppercase"> 
											<strong>Development and Integration of BI, Dashboards, Reports</strong>
										</h3>
										<ul class="list-unstyled">
											<li>
												<p class="grey">
												A solution that consolidates critical information from all across your business footprint and presents it to you in the form of real-time visual data.
												</p>
											</li>
											<li>
												<p class="grey">It helps you acquire better understanding of your business environment and aids in decision-making.
												</p>
											</li>
										</ul>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="solution-content">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-3">
										<div class="solution-number">
											4
										</div>
									</div>
									<div class="col-sm-9">
										<h3 class="text-uppercase"> 
											<strong>Facility management solutions</strong>
										</h3>
										<ul class="list-unstyled">
											<li>
												<p class="grey">
												Facility Management Solution integrates and tracks all information related to the administrative space, infrastructure, people and organization of a facility.
												</p>
											</li>
											<li>
												<p class="grey">It helps you reduce maintenance costs, improve inter-departmental co-ordination and boosts operational efficiency.
												</p>
											</li>
										</ul>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="solution-content">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-3">
										<div class="solution-number">
											5
										</div>
									</div>
									<div class="col-sm-9">
										<h3 class="text-uppercase"> 
											<strong>Distribution management system</strong>
										</h3>
										<ul class="list-unstyled">
											<li>
												<p class="grey">
												This solution keeps track of every activity of Distribution businesses viz. Vehicle Tracking, POS support for sales at all levels in and between warehouses to end-customers, Inventory, Accounts and Sales Management.
												</p>
											</li>
											<li>
												<p class="grey">It comes with an Android based App for Vehicle tracking- an activity very crucial to distribution.
												</p>
											</li>
										</ul>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<h1 class="text-center many-more">
							And many more...
						</h1>	
					</div>
				</div>
				<div class="col-sm-4">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="advantages">
							<div class="container-fluid">
								<div class="col-sm-12">
									<p class="text-center"><strong><u>Advantages</u></strong></p>
									<ul>
										<li>
											<p class="text-justify">
											Keep track of every transaction, taxes, billing and budgets.	
											</p>
										</li>
										<li>
											<p class="text-justify">
											Convert Leads, send Quotations and ensure on-time shipping	
											</p>
										</li>
										<li>
											<p class="text-justify">
											Run an efficient business with accurate information of stock on hand	
											</p>
										</li>
										<li>
											<p class="text-justify">
											Manage your supply chain and stay on top of your purchases	
											</p>
										</li>
										<li>
											<p class="text-justify">
											Manage your bill of materials and plan for production	
											</p>
										</li>
										<li>
											<p class="text-justify">
											Impress your customers by making sure all queries are fulfilled on time using Support Tickets and Visit Tracking	
											</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<section class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="col-sm-6">
							<div class="col-sm-8 col-sm-offset-2">
								<h2 class="section-heading1 text-center">
									<span class="fa fa-users white"></span>
									<span class="span-partners white">Industrial Partners</span>
								</h2>
							</div>	
							<div class="industrial-partners">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/ums.png" alt="ums" class="img-responsive img-center">	
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/google.png" alt="google" class="img-responsive img-center">
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/lt.png" alt="l&t" class="img-responsive img-center">
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/samsung-logo.png" alt="samsung" class="img-responsive img-center">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-sm-8 col-sm-offset-2">
								<h2 class="section-heading1 text-center">
									<span class="fa fa-map-marker white"></span>
									<span class="span-contact white">Contact</span>
								</h2>
							</div>
							<div class="col-sm-12">
								<p class="text-center">
									Office 3 &amp; 4, Sumansudha, Amchi Colony,  
                                    NDA-Pashan Road, Bavdhan, Pune - 411021, 
                                    Maharashtra, India.
								</p>
								<p class="text-center">
									<span class="fa fa-phone">	
									</span> (020) 6570 0800 | 
									<span class="fa fa-envelope"></span> contact@indictranstech.com
								</p>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="copyright">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<h4 class="text-center">
											© 2015 New Indictrans Technologies Pvt Ltd | All copyrights reserved
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="#" alt=sitemap>
								<h4>Sitemap</h4>
							</a>
						</div>
						<div class="col-sm-6">
							<div class="social">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<table class="pull-right">
												<tbody>
													<tr>
														<td class="footer-table">
															<h4 class="follow">
																Follow Us:
															</h4>
														</td>
														<td class="footer-table">
															<ul class="text-center list-inline">
																<li>
																	<a href="#">
																		<span class="fa fa-twitter"></span>
																	</a>
																</li>
																<li>
																	<a href="#">
																		<span class="fa fa-linkedin"></span>
																	</a>
																</li>
																<li>
																	<a href="#">
																		<span class="fa fa-facebook"></span>
																	</a>
																</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</footer>
</body>
</html>	