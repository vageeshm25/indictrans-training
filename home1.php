<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Indictrans</title>
	<!-- Bootstrap css -->
	<link href="css/bootstrap.min.css" rel="stylesheet">	
	<!-- Multiscroll css -->
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.multiscroll.css" /> -->
	<!-- Font awesome -->
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<!-- Custom Css -->
	<link type="text/css" rel="stylesheet" href="css/all.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#"><img src="img/logo/logo.png" alt="Logo" class="img-responsive logo" /></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right tab-links">
		        <li class="active"><a href="#" class="links">Home</a></li>
		        <li><a href="#" class="links">About Us</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Solutions <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">ERP Next</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="#">GIS</a></li>
		          </ul>
		        </li>
		        <li><a href="#" class="links">Career</a></li>
		        <li><a href="#" class="links">Clientele</a></li>
		        <li><a href="#" class="links">Contact</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
	<div class="mobile-wrapper">
		<!-- Responsive slider code start -->
        <section class="mobile-landing-section">
			<div class="container-fluid fill">
				<div class="row slider">
					<div id="myCarousel" class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<div class="screen">
									<img src="img/slider/meeting.jpg" alt="meeting" class="img-responsive" />
    								<section class="inner-section">
    									<div class="container-fluid">
    										<div class="row">
    											<div class="col-sm-4 col-offset-1 text-center box">
    												<h3 class="text-center box-text">Empowering SMEs with Business Intelligence and ERP 
    												Solutions for all its functions…Facilitating insightful decision-making</h3>
    												<a href="#">
    													<div class="arrow"></div>
    												</a>
    											</div>
    										</div>
    									</div>
    								</section>
    							</div>	
							</div>
							<div class="item">
								<div class="screen">
									<img src="img/slider/city.jpg" alt="city" class="img-responsive" />
    								<section class="inner-section">
    									<div class="container-fluid">
    										<div class="row">
    											<div class="col-sm-4 col-offset-1 text-center box">
    												<h3 class="text-center box-text">Ushering in futuristic development in e-governance for Smart Cities using GIS</h3>
    												<a href="#">
    													<div class="arrow"></div>
    												</a>
    											</div>
    										</div>
    									</div>
    								</section>
    							</div>	
							</div>
							<div class="item">
								<div class="screen">
									<img src="img/slider/railway.jpg" alt="railway" class="img-responsive" />
    								<section class="inner-section">
    									<div class="container-fluid">
    										<div class="row">
    											<div class="col-sm-4 col-offset-1 text-center box">
    												<h3 class="text-center box-text">Offering Business Solutions to the likes of Indian Railways, various Municipal 
                                        			Corporations, Manufacturing houses and other large organizations for efficient 
                                        			Planning and Execution </h3>
                                        			<a href="#">
    													<div class="arrow"></div>
    												</a>
    											</div>
    										</div>
    									</div>
    								</section>
    							</div>	
							</div>
							<div class="item">
								<div class="screen">
									<img src="img/slider/iStock.jpg" alt="stock" class="img-responsive" />
    								<section class="inner-section">
    									<div class="container-fluid">
    										<div class="row">
    											<div class="col-sm-4 col-offset-1 text-center box">
    												<h3 class="text-center box-text">Empowering SMEs with Business Intelligence and ERP 
    												Solutions for all its functions…Facilitating insightful decision-making</h3>
    												<a href="#">
    													<div class="arrow"></div>
    												</a>
    											</div>
    										</div>
    									</div>
    								</section>
    							</div>	
							</div>
						</div>
					</div>	
				</div>
			</div>
		</section>
        <!-- Responsive slider code end -->

        <!-- Responsive solution start -->
        <section class="mobile-solutions">
			<div class="container-fluid fill">
				<h1 class="section-heading text-center">
					<span class="fa fa-cogs"></span>
					<span class="span-solution">Solutions</span>
				</h1>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="col-sm-6">
							<div class="col-sm-12">
								<img src="img/services1.png" alt="services" class="img-responsive" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-sm-12 text-center">
								<div class="quote-circle text-center">
    								<span class="fa fa-quote-left"></span>
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<h2 class="text-center text-muted">GIS</h2>
								<h3 class="text-center">Spatial Analysis and Visualization Solutions using open source GIS</h3>
								<p class="grey">Customized Spatial Analysis and Visualization Solutions based on GIS for Good e- Governance.</p>
							</div>
							<div class="col-sm-12 text-center">
								<div class="row">
									<ul class="list-inline text-center">
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>50+ odd implements</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Member of ICG on National GIS</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Appreciated by JNNURM</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Meeting Govt. Mandate</h5>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<button type="button" class="read-more-button">Read More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
        <!-- Responsive solution end -->

        <!-- Responsive erpnext start -->
        <section class="mobile-erpnext">
			<div class="container-fluid fill">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="col-sm-6">
							<div class="col-sm-12 text-center">
								<div class="quote-circle text-center">
    								<span class="fa fa-quote-left"></span>
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<h2 class="text-center text-muted">ERPNext</h2>
								<h3 class="text-center">Business Solutions using ERPNext</h3>
								<p class="grey">ERP solutions for every function of SMEs, Customized to a T.</p>
							</div>
							<div class="col-sm-12 text-center">
								<div class="row">
									<ul class="list-inline text-center">
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>60+ Implements</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Across Industry Domain</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Open Source</h5>
										</li>
										<li class="col-sm-3">
											<img src="img/award-small.png">
											<h5>Cloud, Web, Mobile Support</h5>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<button type="button" class="read-more-button">Read More</button>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-sm-12">
								<img src="img/services2.png" alt="services" class="img-responsive" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
        <!-- Responsive erpnext end -->

        <!-- Responsive clientele start -->
	    <section class="mobile-clientele-section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="col-sm-6 col-sm-offset-3">
							<h1 class="section-heading1 text-center">
        						<span>Clientele</span>
        					</h1>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="clients-logo">
							<div class="container-fluid">
								<table cellpadding="10">
									<tbody>
										<tr>
											<td>
												<img src="img/clientele/ashok01.png" alt="Collectorate of thane">
											</td>
											<td>
												<img src="img/clientele/cequity.png" alt="Hansa Cequity">
											</td>
											<td>
												<img src="img/clientele/frank.png" alt="Knight Frank">
											</td>
											<td>
												<img src="img/clientele/pmpml.png" alt="Pmpml">
											</td>
										</tr>
										<tr>
											<td>
												<img src="img/clientele/MPFD.png" alt="madhya pradesh forest department">
											</td>
											<td>
												<img src="img/clientele/ptp.png" alt="Pune Traffic Police">
											</td>
											<td>
												<img src="img/clientele/ir.png" alt="Indian railways">
											</td>
											<td>
												<img src="img/clientele/maharashtra.png" alt=" maharashtra government">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	    <!-- Responsive clientele end -->

         <!-- Responsive testimonial start -->
		<section class="mobile-testimonials">
			<div class="container-fluid">
				<h1 class="section-heading1 test text-center">
					<span class="fa fa-pencil"></span>
					<span class="span-testimonial">Testimonials</span>
				</h1>
				<div class="row">
					<div class="col-sm-12">
						<div id="carousel-example-generic" class="carousel slide carousel-fade carousel-sync" data-ride="carousel" data-pause="false" data-interval="5000">
							<div class="container-fluid">
								<div class="col-sm-10 col-sm-offset-1">
									<div class="carousel-inner">
										<div class="item active">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/digitales.png" alt="digitales" class="img-responsive" title="digitales">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"New Indictrans delivers the business requirements we need in a competitive market. We at Digitales value"</p>
												<p class="text-right"><i>- Digitales, Australia (Online Music Library)</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
										<div class="item">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/esds.png" alt="esds" class="img-responsive" title="esds">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"We would like to express our satisfaction with the GIS software services provided by Indictrans. Indictrans Team is extremely talented and supportive."</p>
												<p class="text-right"><i>- ESDS Software Solution Pvt Ltd, Nashik, India (Data Center Management)</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
										<div class="item">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/choice.png" alt="choice" class="img-responsive" title="choice">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"Indictrans has been fantastic – specialized inventory control built directly into our ERP is just one of the ways they’ve helped to streamline our business operations."</p>
												<p class="text-right"><i>- Energy Choice Inc., USA (Power Solutions)</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
										<div class="item">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/ptp.png" alt="ptp" class="img-responsive" title="pune traffic police">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"We are glad to issue this letter of appreciation for the pioneering initiative taken by Indictrans in building GIS solution for traffic police department and public access. Apart from the technical competence and leadership skill, Indictrans has shown the commitment to the standards and Free / Open source with a desire to spread  benefits of technology to the masses. Indictrans delivered the software in stipulated time and cost, something which is rare for software project of this complexity."</p>
												<p class="text-right"><i>- Dy. Commissioner of Police, Traffic, Pune</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
										<div class="item">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/wr.png" alt="wr" class="img-responsive" title="western-railway">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"Indictrans has developed SMS Integrated solution for Financial Management. This solution has strengthened the e-governance issues and has provided valuable linkage to the issues of transparency in administration. I wish them all the luck with their endeavors."</p>
												<p class="text-right"><i>- Senior EDP Manager, Western Railway, Churchgate, Mumbai</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
										<div class="item">
											<div class="col-sm-4 text-center">
												<img src="img/testimonials/wr.png" alt="wr" class="img-responsive" title="western-railway">
											</div>
											<div class="col-sm-8 testimonial-content">
												<p class="text-justify">"I thanks Indictrans for their role in implementing Business Solution. Today our department remains glad in Indian Railway, to have such software system running for such long period."</p>
												<p class="text-right"><i>- Sr. DPO, Personnel Department, Western Railway, Mumbai</i></p>
        										<span class="fa fa-quote-left"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						        <span class="sr-only">Previous</span>
						    </a>
						    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						        <span class="sr-only">Next</span>
						    </a>
						</div>	
					</div>
				</div>
			</div>
		</section>
	    <!-- Responsive testimonial end -->

        <!-- Responsive pat start -->
	    <section class="mobile-pat-on-the-back">
			<div class="container-fluid">
				<h1 class="section-heading1 test text-center">
					<span>Pat on the back</span>
				</h1>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="col-sm-3">
							<div class="pat">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="panel">
													<div class="panel-body">
														<img src="img/award-big.png" title="awards" alt="awards" class="img-center img-responsive">
														<h4>Nominee for PM award for good E-governance</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="pat">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="panel">
													<div class="panel-body">
														<img src="img/award-big.png" title="awards" alt="awards" class="img-center img-responsive">
														<h4>SCOTCH award for good E-governance</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="pat">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="panel">
													<div class="panel-body">
														<img src="img/award-big.png" title="awards" alt="awards" class="img-center img-responsive">
														<h4>Nominee for Manthan award</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="pat">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="panel">
													<div class="panel-body">
														<img src="img/award-big.png" title="awards" alt="awards" class="img-center img-responsive">
														<h4>Member of ICG for national GIS</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	    <!-- Responsive pat end -->

        <!-- Responsive footer start -->
	    <section class="mobile-footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="col-sm-6">
							<div class="col-sm-8 col-sm-offset-2">
								<h2 class="section-heading1 text-center">
									<span class="fa fa-users white"></span>
									<span class="span-partners white">Industrial Partners</span>
								</h2>
							</div>	
							<div class="industrial-partners">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/ums.png" alt="ums" class="img-responsive img-center">	
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/google.png" alt="google" class="img-responsive img-center">
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/lt.png" alt="l&t" class="img-responsive img-center">
										</div>
										<div class="col-xs-3 text-center">
											<img src="img/technical partners/samsung-logo.png" alt="samsung" class="img-responsive img-center">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-sm-8 col-sm-offset-2">
								<h2 class="section-heading1 text-center">
									<span class="fa fa-map-marker white"></span>
									<span class="span-contact white">Contact</span>
								</h2>
							</div>
							<div class="col-sm-12">
								<p class="text-center">
									Office 3 &amp; 4, Sumansudha, Amchi Colony,  
                                    NDA-Pashan Road, Bavdhan, Pune - 411021, 
                                    Maharashtra, India.
								</p>
								<p class="text-center">
									<span class="fa fa-phone">	
									</span> (020) 6570 0800 | 
									<span class="fa fa-envelope"></span> contact@indictranstech.com
								</p>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="copyright">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<h4 class="text-center">
											© 2015 New Indictrans Technologies Pvt Ltd | All copyrights reserved
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="#" alt=sitemap>
								<h4>Sitemap</h4>
							</a>
						</div>
						<div class="col-sm-6">
							<div class="social">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<table class="pull-right">
												<tbody>
													<tr>
														<td class="footer-table">
															<h4 class="follow">
																Follow Us:
															</h4>
														</td>
														<td class="footer-table">
															<ul class="text-center list-inline">
																<li>
																	<a href="#">
																		<span class="fa fa-twitter"></span>
																	</a>
																</li>
																<li>
																	<a href="#">
																		<span class="fa fa-linkedin"></span>
																	</a>
																</li>
																<li>
																	<a href="#">
																		<span class="fa fa-facebook"></span>
																	</a>
																</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	    <!-- Responsive footer end -->
	</div>
	
	<!-- <div id="scroll-nav" class="right">
		<ul>
			<li data-tooltip="Indictrans">
				<a href="#first" class="active">
					<span></span>
				</a>
			</li>
			<li data-tooltip="GIS">
				<a href="#second" class="">
					<span></span>
				</a>
			</li>
			<li data-tooltip="ERPNext">
				<a href="#third" class="">
					<span></span>
				</a>
			</li>
			<li data-tooltip="Clientele">
				<a href="#fourth" class="">
					<span></span>
				</a>
			</li>
			<li data-tooltip="Testimonials">
				<a href="#fifth" class="">
					<span></span>
				</a>
			</li>
			<li data-tooltip="Awards">
				<a href="#sixth" class="">
					<span></span>
				</a>
			</li>
			<li data-tooltip="Contact">
				<a href="#seventh" class="">
					<span></span>
				</a>
			</li>
		</ul>
	</div> -->
	<!-- Script start -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>	
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<!-- <script src="js/jquery.easings.min.js"></script> -->
	<!-- <script type="text/javascript" src="js/jquery.multiscroll.min.js"></script> -->
	<script type="text/javascript">
	$(document).ready(function() {
   		// $('#multiscroll').multiscroll();

    // $.fn.multiscroll.moveSectionDown();
     // easing: 'easeInOutSine',
       // navigation: true,
     //   css3: true,
     //   anchors: ['first', 'second']
	});
	</script>
	<!-- Script end -->
</body>
</html>